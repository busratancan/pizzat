﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using PizzaSiparis;

namespace PizzaSiparis.Controllers
{
    public class SiparisController : Controller
    {
        private pizzaEntities db = new pizzaEntities();

        // GET: Siparis1
        public ActionResult Index()
        {
            var siparis = db.Siparis.Include(s => s.Icecek).Include(s => s.Pizza);
            return View(siparis.ToList());
        }

        // GET: Siparis1/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Siparis siparis = db.Siparis.Find(id);
            if (siparis == null)
            {
                return HttpNotFound();
            }
            return View(siparis);
        }

        // GET: Siparis1/Create
        public ActionResult Create()
        {
            ViewBag.icecekID = new SelectList(db.Icecek, "icecekID", "icecekAdi");
            ViewBag.pizzaID = new SelectList(db.Pizza, "pizzaID", "pizzaAdı");
            return View();
        }

        // POST: Siparis1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "siparisID,adiSoyadi,tel,adres,pizzaID,icecekID")] Siparis siparis)
        {
            if (ModelState.IsValid)
            {
                db.Siparis.Add(siparis);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.icecekID = new SelectList(db.Icecek, "icecekID", "icecekAdi", siparis.icecekID);
            ViewBag.pizzaID = new SelectList(db.Pizza, "pizzaID", "pizzaAdı", siparis.pizzaID);
            return View(siparis);
        }

        // GET: Siparis1/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Siparis siparis = db.Siparis.Find(id);
            if (siparis == null)
            {
                return HttpNotFound();
            }
            ViewBag.icecekID = new SelectList(db.Icecek, "icecekID", "icecekAdi", siparis.icecekID);
            ViewBag.pizzaID = new SelectList(db.Pizza, "pizzaID", "pizzaAdı", siparis.pizzaID);
            return View(siparis);
        }

        // POST: Siparis1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "siparisID,adiSoyadi,tel,adres,pizzaID,icecekID")] Siparis siparis)
        {
            if (ModelState.IsValid)
            {
                db.Entry(siparis).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.icecekID = new SelectList(db.Icecek, "icecekID", "icecekAdi", siparis.icecekID);
            ViewBag.pizzaID = new SelectList(db.Pizza, "pizzaID", "pizzaAdı", siparis.pizzaID);
            return View(siparis);
        }

        // GET: Siparis1/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Siparis siparis = db.Siparis.Find(id);
            if (siparis == null)
            {
                return HttpNotFound();
            }
            return View(siparis);
        }

        // POST: Siparis1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Siparis siparis = db.Siparis.Find(id);
            db.Siparis.Remove(siparis);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
